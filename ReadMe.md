# Overview
gtwelve ActiveRecord helps manage your data in angularJS apps. It extends factories and services so they behave similarly to Models and Collections in Backbone. 

# Usage
Open `index.html` to see an example of how ActiveRecord works.
Open `example.js` to see a how `index.html` works.
Open `activerecord.js` to see how the script works.