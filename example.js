// Create the angular module as "app"
var app = angular.module('app', ['activeRecord']);


// Run the app
app.run([function () {
	console.log('App starting');
}]);


// Create a "personModel" factory which returns an "activeModel" object from "ActiveRecord"
app.factory('personModel', ['$http', '$q', 'activeModel', function ($http, $q, activeModel) {
	
	return activeModel.extend({
		_name: 'name',
		_defaults: {
			id: {
				dataType: 'int',
				value: null
			},
			name: {
				dataType: 'string',
				value: null
			},
			position: {
				dataType: 'string',
				value: 'employee'
		 	},
		 	created_at: {
				dataType: 'timestamp',
				value: 'current_timestamp'
		 	},
		 	updated_at: {
				dataType: 'timestamp',
				value: 'current_timestamp'
			}
		},
		_url: function() {
			 return './example_response.json' + (this.id ? '?id='+this.id : '');
		},
		nameAndPosition: function() {
			return this.get('name') + ': ' + this.get('position');
		}
	});
	
}]);


// Create a "peopleService" service which returns a "activeService" object from "ActiveRecord"
app.service('peopleService', ['$http', 'activeService', 'personModel', function ($http, activeService, personModel) {
	
	return angular.extend(angular.copy(activeService), {
		_model: personModel,
		_indexKey: null,
		_url: './example_people.json',
	});
	
}]);


// Create our example controller to run the interactive demo
app.controller('exampleController', ['$scope', 'peopleService', 'personModel', function ($scope, peopleService, personModel) {
	
	peopleService.fetch().then(function(people) {
		$scope.people = people;
	});
	
	$scope.addData = {};
	$scope.addPerson = function($event) {
		$scope.addData.id = Math.floor(Math.random()*999999999);	// Create a new id for the example only. This should be returned by the server and ActiveRecord will automatically update the model once it gets a response.
		$scope.addData.name = $scope.addData.name || 'unknown' ;
		
		//	Normally we would create a new person via peopleService.create method but since we do not have a remote server HTTP PUT will fail. Below is an example of how to use peopleService.create
		/*
		peopleService.create($scope.addData).then(function(model) {
			if (model) {
				$scope.addData = {};
			} else {
				// Do something
			}
		});
		*/
		
		// We will bypass the server and manually add the new person to the peopleService.
		peopleService._setItem($scope.addData);
		$scope.addData = {};
	};
	
	
	$scope.destroy = function($event, model) {
		$event.preventDefault();
		peopleService.destroy(model.id, {local_destroy: true});	// Destroy the model locally and dont sync with remote server.
	};
	
	
	$scope.print = function($event, model) {
		$event.preventDefault();
		console.log(model);
	};
	
}]);

