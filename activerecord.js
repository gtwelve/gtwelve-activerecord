var ac = angular.module('activeRecord', []);


ac.factory('activeModel', ['$http', '$q', function($http, $q) {
	
	var ActiveModel = function ActiveModel() {
		this._init.apply(this, arguments);
	};
	
	ActiveModel.prototype = {
		
		_init: function(data) {
			this._setDefaults();
			this._setData(data || {});
			this.initialize();
		},
		
		_setDefaults: function() {
			if (typeof(this._defaults) == 'object') {
				for (var k in this._defaults) {
					if (!this.isset(k)) {
						this.set(k, this._defaults[k].value);
					}
				}
			}
		},
		
		_setData: function(data) {
			for (var k in data) {
				if (data.hasOwnProperty(k)) {
					this.set(k, data[k]);
				}
			}
			return this;
		},
		
		_getURL: function(options) {
			return typeof(this._url) == 'function' ? this._url(options || {}) : this._url ;
		},
		
		initialize: function() {},
		
		isNew: function() {
			return (this.id == null || typeof(this.id) == 'undefined') ? true : false ;
		},
		
		set: function(key, value) {
			if (typeof(this._defaults) == 'object' && typeof(this._defaults[key]) != 'undefined') {
				if (value === null) {
					value = null;
				} else {
					switch(this._defaults[key].dataType) {
						case 'string':
							value = value.toString();
							break;
						case 'int':
							value = parseInt(value);
							break;
						case 'float':
							value = parseFloat(value);
							break;
						case 'boolean':
							value = ((value === true || value === 'true' || value === 1 || value === '1') ? true : false)
							break;
						case 'timestamp':
							value = (value == 'current_timestamp') ? new Date() : new Date(value);
							break;
					}
				}
			}
			
			this[key] = value;
			return this
		},
		
		get: function(key) {
			return typeof(this[key]) != 'undefined' ? this[key] : null ;
		},
		
		unset: function(key) {
			delete this[key];
		},
		
		isset: function(key) {
			return typeof(this[key]) != 'undefined' ? true : false ;
		},
		
		reset: function() {
			for (var k in this) {
				if (this.hasOwnProperty(k) && typeof this[k] != 'function') {
					this.unset(k);
				}
			}
			return this;
		},
		
		toObject: function() {
			var data = {};
			for (var k in this) {
				if (this.hasOwnProperty(k)) {
					data[k] = this[k] instanceof Date ? (this[k].toJSON()) : this[k] ;
				}
			}
			return data;
		},
		
		fetch: function(ID, options) {
			var _this = this,
				promise = $q.defer();
			
			options = options || {};
			options.silent404 = options.silent404 || true;
			
			$http.get(this._getURL(options) + '/' + ID, options).then(function(result) {
				_this._setData(result.data);
				promise.resolve(_this);	
			}).catch(function(result) {
				promise.resolve(false);
			});
			
			return promise.promise;
		},
		
		save: function(options) {
			var _this = this,
				promise = $q.defer(),
				data = {};
			
			if (typeof(options) == 'object') {
				data = options;
			}
			
			if (typeof(_this._name) != 'undefined') {
				data[_this._name] = _this.toObject();
			} else {
				data = angular.extend(data, _this.toObject());
			}
			
			$http[_this.isNew() ? 'post' : 'put'](_this._getURL(), data).then(function(result) {
				_this._setData(result.data);
				promise.resolve(_this);
			}).catch(function(result) {
				promise.resolve(false);
			});
			
			return promise.promise;
		},
		
		destroy: function() {
			var promise = $q.defer();
			
			$http.delete(this._getURL()).then(function(result) {
				promise.resolve(result);
			}).catch(function(result) {
				promise.resolve(false);
			});
			
			return promise.promise;
		},
		
		// Binds this model to a controllers $scope
		bind: function($name, $scope, $params) {
			var _this = this;
			_this.fetch().then(function(result) {
				$scope[$name] = _this;
			}).catch(function() {
				$scope[$name] = null;
			});
		}
	};
	
	
	/**
	 * Create a subclass.
	 */
	ActiveModel.extend = function(prop) {
		var initializing = false,
			fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/,
			_super = this.prototype;
		
		// Instantiate a base class (but only create the instance, don't run the init constructor)
		initializing = true;
		var prototype = new this();
		initializing = false;
		
		// Copy the properties over onto the new prototype
		for (var name in prop) {
			// Check if we're overwriting an existing function
			prototype[name] = typeof prop[name] == "function" && typeof _super[name] == "function" && fnTest.test(prop[name]) ? (function(name, fn){
				return function() {
					var tmp = this._super;
					
					// Add a new ._super() method that is the same method but on the super-class
					this._super = _super[name];
					
					// The method only need to be bound temporarily, so we remove it when we're done executing
					var ret = fn.apply(this, arguments);
					this._super = tmp;
					
					return ret;
				};
			})(name, prop[name]) : prop[name];
		}
		
		// The dummy class constructor
		function baseClass() {
			// All construction is actually done in the _init method
			if ( !initializing && this._init ) {
				this._init.apply(this, arguments);
			}
		}
		
		baseClass.prototype = prototype;				// Populate our constructed prototype object
		baseClass.prototype.constructor = baseClass;	// Enforce the constructor to be what we expect
		baseClass.extend = arguments.callee;			// And make this class extendable
		
		return baseClass;
	};
	
	
	return ActiveModel;
}]);


/*
 *	
 */
ac.service('activeService', ['$http', '$q', function($http, $q) {
	
	return {
		
		_items: {},
		
		_fetched: null,
		
		_indexKey: null,
		
		_getURL: function(options) {
			return typeof(this._url) == 'function' ? this._url(options || {}) : this._url ;
		},
		
		_setItem: function(data, options) {
			if (data) {
				if (this._indexKey != null && typeof(options[this._indexKey]) != 'undefined') {
					if (typeof(this._items[options[this._indexKey]][data.id]) != 'undefined') {
						this._items[options[this._indexKey]][data.id]._setData(data);
					} else {
						this._items[options[this._indexKey]][data.id] = new this._model(data);
					}
				} else {
					if (typeof(this._items[data.id]) != 'undefined') {
						this._items[data.id]._setData(data);
					} else {
						this._items[data.id] = new this._model(data);
					}
				}
			}
		},
		
		_setIndex: function(options) {
			if (this._indexKey != null && typeof(this._items[options[this._indexKey]]) == 'undefined') {
				this._items[options[this._indexKey]] = {};
			}
		},
		
		_getFetched: function(ID, options) {
			if (this._fetched == null) {
				this._fetched = (this._indexKey == null) ? false : {} ;
			}
			
			if (this._indexKey != null) {
				if (this._fetched[options[this.indexKey]] == true) {
					var items = this.items(options) || false;
					if (items != false) {
						var fetched = (ID != null ? items[ID] : items) || false ;
						var promise = $q.defer();
						promise.resolve(fetched);
						return promise.promise;
					}
				} else {
					this._fetched[options[this.indexKey]] = true;
				}
			} else {
				if (this._fetched == true) {
					var fetched = (ID != null ? this._items[ID] : this._items) || false ;
					if (fetched != false) {
						var promise = $q.defer();
						promise.resolve(fetched);
						return promise.promise;
					}
				} else {
					this._fetched = true;
				}
			}
			
			return false;
		},
		
		_remove_index: function(id, options) {
			if (this._indexKey != null) {
				delete this._items[options[this._indexKey]][id];
			} else {
				delete this._items[id];
			}
		},
		
		items: function(options) {
			if (this._indexKey != null) {
				options = options || {};
				return (typeof(options[this._indexKey]) != 'undefined') ? this._items[options[this._indexKey]] : false ;
			} else {
				return this._items;
			}
		},
		
		fetch: function(options) {
			var _this = this,
				promise = $q.defer();
			
			options = options || {};
			options.silent404 = options.silent404 || true;
			
			var fetched = this._getFetched(null, options);
			if (fetched != false) {
				return fetched;
			}
			
			$http.get(this._getURL(options), options).then(function(result) {
				_this._setIndex(options);
				
				angular.forEach(result.data, function(data, i) {
					_this._setItem(data, options);
				});
				
				promise.resolve(_this.items(options));
			}).catch(function() {
				promise.resolve(false);
			});
			
			return promise.promise;
		},
		
		fetchOne: function(ID, options) {
			var _this = this,
				promise = $q.defer();
			
			options = options || {};
			
			var fetched = this._getFetched(ID, options);
			if (fetched != false) {
				return fetched;
			}
			
			new this._model().fetch(ID).then(function(model) {
				if (model) {
					_this._setIndex(options);
					_this._setItem(model, options);
					promise.resolve(model);
				} else {
					promise.resolve(false);
				}
			});
			
			return promise.promise;
		},
		
		create: function(data, options) {
			var _this = this,
				model = new this._model(data),
				promise = $q.defer();
			
			options = options || {};
			
			model.save().then(function(model) {
				_this._setIndex(options);
				_this._setItem(model, options);
				promise.resolve(model);
			});
			
			return promise.promise;
		},
		
		destroy: function (ID, options) {
			var _this = this,
				model = null,
				promise = $q.defer();
			
			options = options || {};
			
			if (_this._indexKey != null && typeof(_this._items[options[_this._indexKey]]) != 'undefined') {
				model = _this._items[options[_this._indexKey]][ID];
			} else if (typeof(_this._items[ID]) != 'undefined') {
				model = _this._items[ID];
			}
			
			if (model != null) {
				if (typeof(options.local_destroy) != 'undefined' && options.local_destroy == true) {
					_this._remove_index(model.id, options);
				} else {
					model.destroy().then(function(result) {
						if (result != false) {
							_this._remove_index(model.id, options);
						}
						
						promise.resolve(result);
					});
				}
			} else {
				promise.resolve(false);
			}
			
			return promise.promise;
		},
		
		bind: function($name, $scope, options) {
			this.fetch(options).then(function(items) {
				$scope[$name] = items;
			});
		},
		
		// alias for model bind
		bindOne: function($name, $scope, $params) {
			new this._model().bind($name, $scope, $params);
		}
	};
}]);